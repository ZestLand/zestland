<!-- 
   <footer class="row footer_row bebas">
    <div class="container footer_container">
        <div class="col-4 f-col-1">
            <a href="#" class="logo"><img src="<?= image_path; ?>/logo.png" alt=""></a>
        </div>
        <div class="col-4 f-col-2 align-center">
            <p class="f-p-1" >Построение эффективной системы продаж <br>через интернет</p>
            <p class="f-p-2"><a href="#popup/private_policy" class="c-orange js-show-privacy">Политика конфиденциальности</a></p>
        </div>
        <div class="col-4 f-col-3 align-center">
            <p class="f-p-3"><a href="#popup/form-2" class="c-orange">Заказать бесплатный звонок</a></p>
            <p class="f-p-4"><span class="sn-title">Присоединяйтесь к нам в соцсетях</span></p>
            <p class="f-p-5"><span class="sn-links">
                <a href="#" class="sn-link"><img src="<?= icon_path; ?>/sn-1.png" alt=""></a>
                <a href="#" class="sn-link"><img src="<?= icon_path; ?>/sn-2.png" alt=""></a>
                <a href="#" class="sn-link"><img src="<?= icon_path; ?>/sn-3.png" alt=""></a>
                <a href="#" class="sn-link"><img src="<?= icon_path; ?>/sn-4.png" alt=""></a>
            </span></p>

        </div>
    </div>
</footer>

<style> .dn{ display: none; } </style>
<div id="private_policy" class="dn">
   <div class="policy__cnt">
           <div class="policy__main-title"><h3 class="bebas">Политика конфиденциальности</h3></div>
       
       <div class="policy__title">Какая информация подлежит сбору</div>
       <div class="policy__desc">
           <p>Сбору подлежат только сведения, обеспечивающие возможность поддержки обратной связи с пользователем. Некоторые действия пользователей автоматически сохраняются в журналах сервера: IP-адрес; данные о типе браузера, надстройках, времени запроса и т. д. </p>
       </div>
       
       <div class="policy__title">Как используется полученная информация </div>
       <div class="policy__desc">
           <p>Сведения, предоставленные пользователем, используются для связи с ним, в том числе для направления уведомлений об изменении статуса заявки. </p>
       </div>
       
       <div class="policy__title">Управление личными данными</div>
       <div class="policy__desc">
           <p>Личные данные доступны для просмотра, изменения и удаления в личном кабинете пользователя. В целях предотвращения случайного удаления или повреждения данных информация хранится в резервных копиях в течение 30 дней и может быть восстановлена по запросу пользователя. </p>
       </div>
       
       <div class="policy__title">Предоставление данных третьим лицам</div>
       <div class="policy__desc">
           <p>Личные данные пользователей могут быть переданы лицам, не связанным с настоящим сайтом, если это необходимо: для соблюдения закона, нормативно-правового акта, исполнения решения суда; для выявления или воспрепятствования мошенничеству; для устранения технических неисправностей в работе сайта; для предоставления информации на основании запроса уполномоченных государственных органов.</p>
       </div>
       
       <div class="policy__title">Безопасность данных</div>
       <div class="policy__desc">
           <p>Администрация сайта принимает все меры для защиты данных пользователей от несанкционированного доступа, в частности: регулярное обновление служб и систем управления сайтом и его содержимым; шифровка архивных копий ресурса; регулярные проверки на предмет наличия вредоносных кодов; использование для размещения сайта виртуального выделенного сервера. </p>
       </div>
       
       <div class="policy__title">Изменения</div>
       <div class="policy__desc">
           <p>Обновления политики конфиденциальности публикуются на данной странице. Для удобства пользователей все версии политики конфиденциальности подлежат сохранению в архивных файлах.</p>
       </div>
    </div>
</div>

<div id="form-1" class="dn">
    <?= do_shortcode('[contact-form-7 id="54" title="Форма попап"]'); ?>
</div>
   
<div id="form-2" class="dn">
    
</div>

<div class="z-popup">
    <div class="z-popup bg js-close-popup"></div>
    <div class="z-popup__cnt">
        <?= do_shortcode('[contact-form-7 id="55" title="Форма попап (звонок)"]'); ?>
    </div>
</div>
-->
<? wp_footer(); ?>
</body>
</html>
