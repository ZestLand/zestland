<?
/**
 * Шаблон обычной страницы (page.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); ?>

<?
	if ( have_posts() ) the_post();
	the_content();
	//get_sidebar();
?>
<? if(is_active_sidebar('sidebar')) $ext_class='has-sidebar sidebar-is-active'; else $ext_class=''; ?>
<? get_footer(); ?>
