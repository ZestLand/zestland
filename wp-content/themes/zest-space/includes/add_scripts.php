<?
add_action('wp_footer', 'add_scripts');
function add_scripts() {
    if(is_admin()) return false;
    wp_deregister_script('jquery');
    
    wp_enqueue_script('jquery','//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js','','',true);
    wp_enqueue_script('bx-slider_js', get_template_directory_uri().'/assets/libs/bxslider-4/jquery.bxslider.min.js','','',true);
    wp_enqueue_script('fancybox_js', get_template_directory_uri().'/assets/libs/fancybox-3/jquery.fancybox.js','','',true);
    wp_enqueue_script('routie_js', get_template_directory_uri().'/assets/libs/routie/routie.min.js','','',true);
    wp_enqueue_script('main_js', get_template_directory_uri().'/assets/js/main'.JS_MIN.'.js?js_cache='.JS_CACHE,'','',true);
}