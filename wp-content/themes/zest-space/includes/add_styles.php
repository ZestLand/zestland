<?
add_action('wp_print_styles', 'add_styles');
function add_styles() {
    if(is_admin()) return false;

    wp_enqueue_style( 'font-bebas', get_template_directory_uri().'/assets/fonts/BebasNeue/font.css');
    wp_enqueue_style( 'fancybox_css', get_template_directory_uri().'/assets/libs/fancybox-3/jquery.fancybox.css' );
    wp_enqueue_style( 'bx-slider_css', get_template_directory_uri().'/assets/libs/bxslider-4/jquery.bxslider.css' );
    wp_enqueue_style( 'main_styles', get_template_directory_uri().'/assets/css/styles'.CSS_MIN.'.css?css_cache='.CSS_CACHE );
}
