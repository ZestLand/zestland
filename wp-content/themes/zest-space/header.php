<!DOCTYPE html>
<html <? language_attributes(); ?>>
<head>
	<meta charset="<? bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<? /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<? bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<? bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<? bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<? bloginfo( 'pingback_url' ); ?>" />

	 <!--[if lt IE 9]>
	 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	 <![endif]-->

	<title><? typical_title(); ?></title>
	<? wp_head(); ?>
</head>
<body <? body_class(); ?>>
	
        <!--<header class="row h bebas">
			<div class="container h_container">
					<a href="#" class="h_logo link-logo"></a>
					<div class="h_nav-and-offer">
							<nav class="h_nav">
									<ul>
											<li><a href="#"><span>главная</span></a></li>
											<li><a href="#"><span>о компании</span></a></li>
											<li><a href="#"><span>запуск “под ключ”</span></a></li>
											<li><a href="#"><span>подарки</span></a></li>
											<li><a href="#"><span>стоимость</span></a></li>
											<li><a href="#"><span>контакты</span></a></li>
									</ul>
							</nav>
							<div class="h_offer">
									<h1>Построение эффективной системы продаж <br>через интернет</h1>
							</div>
					</div>
					<div class="h_contacts">
							<a class="link-phone" href="#">info@zestland.ru</a>
							<a class="link-email" href="#">8-962-503-23-33</a>
					</div>
			</div>
	</header>
-->