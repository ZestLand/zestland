<?
define('DEV', true);

if(DEV) define('CSS_CACHE', time());
else define('CSS_CACHE', 'a1');
define('IS_CSS_MIN', false);
if(IS_CSS_MIN) define('CSS_MIN', '.min');
else define('CSS_MIN', '');

if(DEV) define('JS_CACHE', time());
else define('JS_CACHE', 'a1');  
define('IS_JS_MIN', false);
if(IS_JS_MIN) define('JS_MIN', '.min');
else define('JS_MIN', '');

define('image_path', get_template_directory_uri(). '/assets/images');
define('icon_path', get_template_directory_uri(). '/assets/icons');

function typical_title() {
	global $page, $paged;
	wp_title('', true, 'right');
	$site_description = get_bloginfo('description', 'display');
	if ($site_description && (is_home() || is_front_page())) echo " | $site_description";
	if ($paged >= 2 || $page >= 2) echo ' | '.sprintf(__( 'Страница %s'), max($paged, $page));
}

register_nav_menus(array(
	'top' => 'Верхнее',
	'bottom' => 'Нижнее'
));

add_theme_support('post-thumbnails');
set_post_thumbnail_size(300, 300);
//add_image_size('big-thumb', 400, 400, true);

register_sidebar(array(
	'name' => 'Сайдбар',
	'id' => "sidebar",
	'description' => 'Обычная колонка в сайдбаре',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => "</div>\n",
	'before_title' => '<span class="widgettitle">',
	'after_title' => "</span>\n",
));

function pagination() {
	global $wp_query;
	$big = 999999999;
	$links = paginate_links(array(
		'base' => str_replace($big,'%#%',esc_url(get_pagenum_link($big))),
		'format' => '?paged=%#%',
		'current' => max(1, get_query_var('paged')),
		'type' => 'array',
		'prev_text'    => 'Назад',
    	'next_text'    => 'Вперед',
		'total' => $wp_query->max_num_pages,
		'show_all'     => false,
		'end_size'     => 15,
		'mid_size'     => 15,
		'add_args'     => false,
		'add_fragment' => '',
		'before_page_number' => '',
		'after_page_number' => ''
	));
 	if( is_array( $links ) ) {
	    echo '<ul class="pagination">';
	    foreach ( $links as $link ) {
	    	if ( strpos( $link, 'current' ) !== false ) echo "<li class='active'>$link</li>";
	        else echo "<li>$link</li>";
	    }
	   	echo '</ul>';
	 }
}

include 'includes/zest_parameters.php';
include 'includes/shortcodes.php';
include 'includes/classes.php';
include 'includes/add_scripts.php';
include 'includes/add_styles.php';
