jQuery(function($){
    var $body = $('body');
    $body.on('focus', '.labels-as-placeholders .lph input', function(e){
        var $this = $(this);
        var $form_item = $this.closest('.form-item');
        $form_item.addClass('focused label-out');
    });
        
    $body.on('focusout', '.labels-as-placeholders .lph input', function(e){
        var $this = $(this);
        var $form_item = $this.closest('.form-item');
        var input_val = $.trim($this.attr('value'));
        $form_item.removeClass('focused');
        if(input_val == ''){
            $form_item.removeClass('label-out');
        }
    });
        
    var bx_config = {
        pager: false
    };
    $('.case_slider').bxSlider(bx_config);
        
    /*
    $body.on('click', '.js-show-privacy', function(e){
        var block_id = '#private_policy';
        var $block = $(block_id);
        //$.fancybox.open($block.html());
        //e.preventDefault();
        //return false;
    });
    */
    
    routie('popup/:id', function(id) {
        var block_id = '#' + id;
        var $block = $(block_id);
        $.fancybox.open($block.html());
    });
});