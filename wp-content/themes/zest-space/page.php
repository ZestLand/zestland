<!DOCTYPE html>
<html <? language_attributes(); ?>>
<head>
	<meta charset="<? bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<? /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<? bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<? bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<? bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<? bloginfo( 'pingback_url' ); ?>" />

	 <!--[if lt IE 9]>
	 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	 <![endif]-->

	<title><? typical_title(); ?></title>
	<? wp_head(); ?>
</head>
<body <? body_class(); ?>>

<?
	if ( have_posts() ) the_post();
	the_content();
	//get_sidebar();
?>
<? //if(is_active_sidebar('sidebar')) $ext_class='has-sidebar sidebar-is-active'; else $ext_class=''; ?>
<? wp_footer(); ?>
</body>
</html>